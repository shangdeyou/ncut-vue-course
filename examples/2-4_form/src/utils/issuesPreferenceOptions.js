export default [
  {
    "label": "國防外交",
    "value": 0
  },
  {
    "label": "教育文化",
    "value": 1
  },
  {
    "label": "法治人權",
    "value": 2
  },
  {
    "label": "經濟產業",
    "value": 3
  },
  {
    "label": "醫療照護",
    "value": 4
  },
  {
    "label": "國家發展",
    "value": 5
  },
  {
    "label": "財政金融",
    "value": 6
  },
  {
    "label": "環境衛生",
    "value": 7
  },
  {
    "label": "勞動弱勢",
    "value": 8
  },
  {
    "label": "科技能源",
    "value": 9
  },
  {
    "label": "交通建設",
    "value": 10
  },
  {
    "label": "社會治安",
    "value": 1
  },
]
