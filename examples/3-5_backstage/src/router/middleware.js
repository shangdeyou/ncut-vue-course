import store from '@/store/index'

export default function (to, from, next) {
  // -- 登入權限處理 --
  let promiseIsLogin = store.dispatch('actionCheckLoginStatus')
  promiseIsLogin
    .then(data => {
      if (data.success === true) {
        // -- 已登入 --
        if (to.name === 'login') {
          next({ name: 'index' })
        } else {
          next()
        }
      } else {
        // -- 未登入 --
        if (to.name === 'login') {
          next()
        } else {
          next({ name: 'login' })
        }
      }
    })
    .catch(e => {
      next()
    })
}
