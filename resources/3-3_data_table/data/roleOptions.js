export default [
  {
    label: '系統管理者',
    value: 1
  },
  {
    label: '公司管理者',
    value: 2
  },
  {
    label: '一般使用者',
    value: 3
  }
]
