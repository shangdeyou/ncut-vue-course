export default [
  {
    index: 'school_name',
    title: '單位名稱',
    fixed: false,
    width: 200
  },
  {
    index: 'user_name',
    title: '使用者姓名',
    fixed: false
  },
  {
    index: 'role_name',
    title: '角色',
    fixed: false,
    width: 120
  },
  {
    index: 'email',
    title: 'Email',
    fixed: false,
    width: 180
  },
  {
    index: 'apply_datetime',
    title: '申請日期',
    fixed: false,
    width: 160
  },
  {
    index: 'account_active',
    title: '啟用狀態',
    fixed: false,
    width: 180
  }
]
