export default [
  {
    label: '請選擇',
    value: 0
  },
  {
    label: '勤益科技大學',
    value: 1
  },
  {
    label: '朝陽科技大學',
    value: 2
  },
  {
    label: '台中科技大學',
    value: 3
  },
  {
    label: '逢甲大學',
    value: 4
  },
  {
    label: '東海大學',
    value: 5
  },
  {
    label: '中興大學',
    value: 6
  },
  {
    label: '靜宜大學',
    value: 7
  },
  {
    label: '中國醫藥大學',
    value: 8
  },
  {
    label: '中山醫學大學',
    value: 9
  }
]
