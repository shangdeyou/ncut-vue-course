import { Message, Notification } from 'element-ui';
import axios from 'axios'

// const successNotify = (title) => {
//   Notification.success({
//     title
//   })
// }

// const successMessage = (message) => {
//   Message.success({
//     message
//   })
// }

const errorMessage = (message = '未知錯誤，請確認網路狀態或聯絡系統管理員') => {
  Message.error({
    message
  })
}

export default {
  apiUrl: process.env.VUE_APP_API_URL,
  // 取得使用者資訊
  async usersGet (params) {
    let result = await axios
      .post(`${this.apiUrl}/api/v1/users.get`, params)
      .then((response) => {
        if (!response.data.success) {
          errorMessage(response.data.message || '')
        }
        return response.data
      })
      .catch((error) => {
        errorMessage()
        return error
      })
    return result
  },
  // 取得學校選單
  async schoolsGet () {
    let result = await axios
      .get(`${this.apiUrl}/api/v1/schools.get`)
      .then((response) => {
        if (!response.data.success) {
          errorMessage(response.data.message || '')
        }
        return response.data
      })
      .catch((error) => {
        errorMessage()
        return error
      })
    return result
  },
}
